package com.example.belval2022.ui.events;

public class Event {
    private String name;
    private String date;
    private String description;
    private String location;
    private double latitude;
    private double longitude;


    public Event(String name, String date, String location, String description,double latitude, double longitude) {
        this.name = name;
        this.date = date;
        this.location = location;
        this.description = description;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    // get the name of an event
    public String getName() {
        return name;
    }
    // get the date of an event
    public String getDate() {
        return date;
    }
    // get the address of an event
    public String getLocation() {
        return location;
    }
    // get the description of an event
    public String getDescription() {
        return description;
    }
    // get the latitude of an event's location
    public double getLatitude(){ return latitude; }
    
    // get the longitude of an event's location
    public double getLongitude(){ return longitude; }

}


    
