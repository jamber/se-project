package com.example.belval2022.ui.community;

import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.denzcoskun.imageslider.models.SlideModel;
import com.example.belval2022.Information1;
import com.example.belval2022.R;
import com.example.belval2022.ui.map.MapFragment;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.security.Security;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static android.app.Activity.RESULT_OK;

public class CommunityFragment extends Fragment {

    private CommunityViewModel notificationsViewModel;
    private int IMAGE_REQUEST = 21;
    private Bitmap bitmap = null;
    private Uri imagePath = null;
    private File image = null;
    SharedPreferences.Editor edit;
    SharedPreferences sharedPreferences;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        notificationsViewModel =
                new ViewModelProvider(this).get(CommunityViewModel.class);
        View root = inflater.inflate(R.layout.fragment_community, container, false);

        Button registerButton = (Button) getActivity().findViewById(R.id.registerButton);
        ImageButton imageButton = (ImageButton) getActivity().findViewById(R.id.imageUploaderButton);

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());

        if(!sharedPreferences.getString("username", "null").equals("null")){
            final FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.chat_fragment, new ChatFragment());
            ft.setReorderingAllowed(true);
            ft.commit();
        }else{
            edit = sharedPreferences.edit();
            edit.clear();
            edit.apply();
        }

        notificationsViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                getActivity().findViewById(R.id.imageUploaderButton).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent();
                        intent.setType("image/*");
                        intent.setAction(Intent.ACTION_GET_CONTENT);
                        startActivityForResult(intent, IMAGE_REQUEST);
                    }
                });
                getActivity().findViewById(R.id.registerButton).setOnClickListener(new View.OnClickListener() {
                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                    @Override
                    public void onClick(View v) {
                        EditText username = (EditText) getActivity().findViewById(R.id.usernameInput);

                        if(!username.getText().toString().equals("")){
                            String url;

                            OkHttpClient client = new OkHttpClient();
                            edit = sharedPreferences.edit();
                            edit.putString("username", username.getText().toString());
                            edit.apply();

                            int SDK_INT = android.os.Build.VERSION.SDK_INT;
                            if (SDK_INT > 8)
                            {
                                StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                                        .permitAll().build();
                                StrictMode.setThreadPolicy(policy);

                                url = "http://10.0.2.2:3000/reg";
                                final MediaType MEDIA_TYPE_PNG = MediaType.parse("image/png");

                                RequestBody requestBody = new MultipartBody.Builder()
                                        .setType(MultipartBody.FORM)
                                        .addFormDataPart("username", username.getText().toString())
                                        .addFormDataPart("image", String.valueOf(getRandomImage()))
                                        .build();

                                Request request = new Request.Builder()
                                        .url(url)
                                        .post(requestBody)
                                        .build();


                                    client.newCall(request).enqueue(new Callback() {
                                        @Override
                                        public void onFailure(Call call, IOException e) {
                                            e.printStackTrace();
                                        }

                                        @Override
                                        public void onResponse(Call call, Response response) throws IOException {
                                            if (response.isSuccessful()) {
                                                String myresponse = response.body().string();
                                                JSONObject o = null;
                                                try {
                                                    o = new JSONObject(myresponse);
                                                    String userId = o.get("insertId").toString();
                                                    System.out.println(userId);
                                                    edit = sharedPreferences.edit();
                                                    edit.putString("userId", userId);
                                                    edit.apply();

                                                    final FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                                                    ft.replace(R.id.chat_fragment, new ChatFragment(), "chatFrag");
                                                    ft.setReorderingAllowed(true);
                                                    ft.commit();
                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }

                                            }
                                        }

                                    });

                            }

                        }else{
                            if(username.getText().toString().equals("")) {
                                redAlertMessage("Please enter a name");
                            }else{
                                redAlertMessage("Please select a profile picture");
                            }
                        }
                    }
                });
            }

        });


        return root;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data){
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == IMAGE_REQUEST && resultCode == RESULT_OK && data != null){
            System.out.println("Uploaded");
            imagePath = data.getData();
            try {
                bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), imagePath);
                ImageButton avatarImage = (ImageButton) getActivity().findViewById(R.id.imageUploaderButton);
                avatarImage.setImageBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    public int getRandomImage(){
        return (int) (Math.random() * 3) + 1;
    }
    //Customer Alert Message
    public void redAlertMessage(String text){
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.toast_layout, (ViewGroup) this.getView().findViewById(R.id.toast_root));

        TextView toastText= layout.findViewById(R.id.toast_text);
        toastText.setText(text);

        Toast toast = new Toast(this.getContext());
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);

        toast.show();
    }
    @Override
    public void onResume(){
        super.onResume();
    }

    @Override
    public void onStop(){
        super.onStop();
    }
}