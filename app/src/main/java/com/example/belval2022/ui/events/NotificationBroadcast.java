package com.example.belval2022.ui.events;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.example.belval2022.R;

public class NotificationBroadcast extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, "notify")
        // choosing an icon for the notification
        .setSmallIcon(R.drawable.ic_notifications_black_24dp)
        // defining the title of the notification
         .setContentTitle("Event Reminder")
         // defining the content of the notification
        .setContentText("That's a reminder of an Esch 2022 event")
         .setPriority(NotificationCompat.PRIORITY_DEFAULT);

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);

        notificationManager.notify(2, builder.build());


    }
}
