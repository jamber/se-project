package com.example.belval2022.ui.community;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.belval2022.R;

import org.w3c.dom.Text;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.ViewHolder> {
    private ArrayList<ChatUser> chatItems;

    public ChatAdapter(ArrayList<ChatUser> chatItems){
        this.chatItems = chatItems;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView usernameTxt;
        private TextView messageTxt;
        private ImageView avatar;

        public ViewHolder(final View view){
            super(view);
            usernameTxt = view.findViewById(R.id.chat_username);
            messageTxt = view.findViewById(R.id.chat_message);
            avatar = view.findViewById(R.id.chat_avatar);
        }
    }

    @NonNull
    @Override
    public ChatAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_items, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ChatAdapter.ViewHolder holder, int position) {
        String username = chatItems.get(position).getUsername();
        String message = chatItems.get(position).getMessage();
        Bitmap avatar = chatItems.get(position).getImage();

        holder.usernameTxt.setText(username);
        holder.messageTxt.setText(message);
        holder.avatar.setImageBitmap(avatar);
    }

    @Override
    public int getItemCount() {
        return chatItems.size();
    }
}
