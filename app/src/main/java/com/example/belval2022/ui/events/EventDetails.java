package com.example.belval2022.ui.events;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.belval2022.R;
import com.example.belval2022.ui.map.MapFragment;

import java.net.MalformedURLException;
import java.util.Calendar;

// This class reprsents the activity of events details (description, address...)
public class EventDetails extends AppCompatActivity {
    private Button locationButton;
    private Button reminderButton;
    private Double latitude;
    private Double longitude;
    private String eventDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.event_details);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        // code for the text
        TextView nameTxt = (TextView ) findViewById(R.id.nameTextView);
        TextView descriptionTxt = (TextView ) findViewById(R.id.descriptionTextView);
        TextView dateTxt = (TextView ) findViewById(R.id.dateTextView);
        TextView timeTxt = (TextView ) findViewById(R.id.timeTextView);
        // Initializing the event's attributes
        String eventName = "not defined";
        String eventDescription = "not defined";
         eventDate = "not defined";
        String eventTime = "not defined";

        // create a Bundle to get the data
        Bundle extras = getIntent().getExtras();
        if(extras != null){
            // getting all data of an event (attributes)
            eventName = extras.getString("name");
            eventDescription = extras.getString("description");
            eventDate = extras.getString("date");
            eventTime = extras.getString("time");
            latitude = extras.getDouble("latitude");
            longitude = extras.getDouble("longitude");
        }
        // show the data of the event on its corresponding text view
        nameTxt.setText( eventName);
        descriptionTxt.setText(eventDescription);
        dateTxt.setText(eventDate);
        timeTxt.setText(eventTime);


        // code for the buttons
        // event location button
        locationButton = findViewById(R.id.locationButton);
        locationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showLocation();
            }
        });

        // set reminder button
        createNotificationChannel();
        reminderButton = findViewById(R.id.reminderButton);
        reminderButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            
                Intent intent = new Intent(EventDetails.this, NotificationBroadcast.class);
                PendingIntent pendingIntent =  PendingIntent.getBroadcast(EventDetails.this,0, intent,0);

                AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
                long timeButtonClicked = System.currentTimeMillis();
                long tenSeconds= 1000 * 10;

                String[] date = splitDate(eventDate);
                int month = 0;
                if(isNumeric(date[0])) {
                    switch (date[1]) {
                        case "January":
                            month = 1;
                            break;
                        case "Febuary":
                            month = 2;
                            break;
                        case "March":
                            month = 3;
                            break;
                        case "April":
                            month = 4;
                            break;
                        case "Mai":
                            month = 5;
                            break;
                        case "June-August":
                        case "June":
                            month = 6;
                            break;
                        case "July":
                            month = 7;
                            break;
                        case "August":
                            month = 8;
                            break;
                        case "September":
                            month = 9;
                            break;
                        case "October":
                            month = 10;
                            break;
                        case "November":
                            month = 11;
                            break;
                        case "December":
                            month = 12;
                            break;
                        default:

                            break;
                    }

                    Calendar cal = Calendar.getInstance();
                    cal.setTimeInMillis(System.currentTimeMillis());
                    cal.clear();
                    cal.set(Integer.valueOf(date[2]), month, Integer.valueOf(date[0]));

                    alarmManager.set(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(),pendingIntent);
                }else{
                    redAlertMessage("A day for this event hasn't been defined yet");
                }
            }
        });
    }
    //check if string is numeric
    public static boolean isNumeric(String str) {
        if (str == null) {
            return false;
        }
        try {
            double d = Double.parseDouble(str);
        } catch (NumberFormatException e) {
            return false;
        }
        return true;
    }
    //Customer Alert Message
    public void redAlertMessage(String text){
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.toast_layout, (ViewGroup) this.findViewById(R.id.toast_root));

        TextView toastText= layout.findViewById(R.id.toast_text);
        toastText.setText(text);

        Toast toast = new Toast(EventDetails.this.getApplicationContext());
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);

        toast.show();
    }
    //splits the date string to get individual elements
    public String[] splitDate(String date){
        String[] arrOfStr = date.split(" ", 3);
        return arrOfStr;
    }
    public void showLocation(){
        Intent intent = new Intent();
        intent.putExtra("lat", latitude);
        intent.putExtra("long", longitude);
        setResult(Activity.RESULT_OK, intent);
        finish();
    }

    //method for set reminder button
    public void createNotificationChannel(){

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            CharSequence name = "EventReminderChannel";
            String description = "Channel for event reminder";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel =  new NotificationChannel("notify", name, importance);
            channel.setDescription(description);

            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);


        }


    }


}
