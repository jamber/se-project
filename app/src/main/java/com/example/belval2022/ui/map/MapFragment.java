package com.example.belval2022.ui.map;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Location;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDialogFragment;
import androidx.fragment.app.Fragment;

import com.example.belval2022.Information1;
import com.example.belval2022.R;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.mapbox.android.core.permissions.PermissionsListener;
import com.mapbox.android.core.permissions.PermissionsManager;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.location.LocationComponent;
import com.mapbox.mapboxsdk.location.LocationComponentActivationOptions;
import com.mapbox.mapboxsdk.location.modes.CameraMode;
import com.mapbox.mapboxsdk.location.modes.RenderMode;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.mapboxsdk.maps.Style;
import com.mapbox.mapboxsdk.plugins.annotation.OnSymbolClickListener;
import com.mapbox.mapboxsdk.plugins.annotation.Symbol;
import com.mapbox.mapboxsdk.plugins.annotation.SymbolManager;
import com.mapbox.mapboxsdk.plugins.annotation.SymbolOptions;
import com.mapbox.mapboxsdk.utils.BitmapUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;


import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class MapFragment extends Fragment implements PermissionsListener {

    private PermissionsManager permissionsManager;
    private MapViewModel dashboardViewModel;
    private MapboxMap mapboxMap;
    private MapView mapView;
    private LocationComponent locationComponent;
    private ArrayList<Marker> markers = new ArrayList<Marker>();

    public MapFragment() throws MalformedURLException {
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Mapbox.getInstance(this.getContext(), getString(R.string.mapbox_access_token));
        View root = inflater.inflate(R.layout.fragment_map, container, false);

        //Http request
        OkHttpClient client = new OkHttpClient();
        String url = "http://10.0.2.2:3000/getMarkers";

        Request request = new Request.Builder()
                .url(url)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if(response.isSuccessful()){
                    String myResponse = response.body().string();
                    try {
                        JSONArray a = new JSONArray(myResponse);
                        for (int i = 0; i < a.length(); i++){
                            JSONObject o = a.getJSONObject(i);

                            int markerId = o.getInt("ID");
                            String markerType = o.getString("Type");
                            double markerLat = o.getDouble("Lat");
                            double markerLong = o.getDouble("Long");

                            Marker m = new Marker(markerId, markerType, markerLat, markerLong);

                            markers.add(m);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    response.body().close();

                    MapFragment.this.getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                        }
                    });
                }
            }
        });



        mapView = root.findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(new OnMapReadyCallback() {

            @Override
            public void onMapReady(@NonNull MapboxMap mapboxMap) {
                mapboxMap.setStyle(new Style.Builder().fromUri("mapbox://styles/louisweber/cknbqhjj00vtp17n0n0jhsumf"), new Style.OnStyleLoaded() {
                    @Override
                    public void onStyleLoaded(@NonNull Style style) {
                        MapFragment.this.mapboxMap = mapboxMap;

                        //Adding custom marker images
                        addMakerImages(style);

                        //Placing the makers on the map
                        placeMarkers(style);

                        //User Location functionality
                        enableLocationComponent(style);

                        //Get the show location button data
                        if (getArguments() != null) {
                            double latitude;
                            double longitude;
                            latitude = getArguments().getDouble("lat");
                            longitude = getArguments().getDouble("long");

                            CameraPosition position = new CameraPosition.Builder()
                                    .target(new LatLng(latitude, longitude))
                                    .build();

                            //Camera animation to the new position
                            mapboxMap.animateCamera(CameraUpdateFactory.newCameraPosition(position), 2000);
                        }
                    }
                });
            }
        });

        //Show Location button event listener
        root.findViewById(R.id.userLocationButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CameraPosition position = new CameraPosition.Builder()
                        .target(new LatLng(getLastLocation().getLatitude(), getLastLocation().getLongitude()))
                        .build();

                //Camera animation to the new position
                mapboxMap.animateCamera(CameraUpdateFactory.newCameraPosition(position), 2000);
            }
        });
        root.findViewById(R.id.modeSwitcher).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Switch lazyMode = (Switch) getActivity().findViewById(R.id.modeSwitcher);

                if(lazyMode.isChecked()){
                    switchAlertMessage("Switched to lazy mode");
                }else {
                    switchAlertMessage("Switched to default mode");
                }
            }
        });

        return root;
    }

    @SuppressWarnings( {"MissingPermission"})
    private void enableLocationComponent(@NonNull Style loadedMapStyle) {
        // Check if permissions are enabled and if not request
        if (PermissionsManager.areLocationPermissionsGranted(this.getContext())) {
            acceptedPermissionMode();
            // Get an instance of the component
            this.locationComponent = mapboxMap.getLocationComponent();

            // Activate with options
            locationComponent.activateLocationComponent(
                    LocationComponentActivationOptions.builder(this.getContext(), loadedMapStyle).build());

            // Enable to make component visible
            locationComponent.setLocationComponentEnabled(true);
            // Set the component's camera mode
            locationComponent.setCameraMode(CameraMode.TRACKING);

            // Set the component's render mode
            locationComponent.setRenderMode(RenderMode.COMPASS);
        } else {
            rejectedPermissionMode();
            //Request a permission popup
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 12);
        }
    }

    //Customer Alert Message
    public void redAlertMessage(String text){
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.toast_layout, (ViewGroup) this.getView().findViewById(R.id.toast_root));

        TextView toastText= layout.findViewById(R.id.toast_text);
        toastText.setText(text);

        Toast toast = new Toast(this.getContext());
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);

        toast.show();
    }

    //Customer Alert Message
    public void switchAlertMessage(String text){
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.switch_message, (ViewGroup) this.getView().findViewById(R.id.toast_root));

        TextView toastText= layout.findViewById(R.id.toast_text);
        toastText.setText(text);

        Toast toast = new Toast(this.getContext());
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);

        toast.show();
    }

    //This method will add custom marker images to the style layer
    public void addMakerImages(Style style){
        Bitmap iconBlue = BitmapUtils.getBitmapFromDrawable(getResources().getDrawable(R.drawable.ic_marker_icon_blue));
        Bitmap iconYellow = BitmapUtils.getBitmapFromDrawable(getResources().getDrawable(R.drawable.ic_marker_icon_yellow));
        Bitmap iconGreen = BitmapUtils.getBitmapFromDrawable(getResources().getDrawable(R.drawable.ic_marker_icon_green));

        style.addImage("blue-icon-id", iconBlue);
        style.addImage("yellow-icon-id", iconYellow);
        style.addImage("green-icon-id", iconGreen);
    }

    //Places the marker onto the map and all the marker events
    public void placeMarkers(Style style){
        SymbolManager symbolManager = new SymbolManager(mapView, mapboxMap, style);
        symbolManager.setIconAllowOverlap(true);
        symbolManager.setIconIgnorePlacement(true);

        for (int i = 0; i < markers.size(); i++){
            JsonElement markerData = JsonParser.parseString("{id: "+markers.get(i).getId()+", type: "+markers.get(i).getType()+"}").getAsJsonObject();
            String icon = "";

            if(markers.get(i).getType().equals("uni")){
                icon = "blue-icon-id";
            }else if(markers.get(i).getType().equals("Res")){
                icon = "yellow-icon-id";
            }else if(markers.get(i).getType().equals("Culture")){
                icon = "green-icon-id";
            }

            Symbol symbol = symbolManager.create(new SymbolOptions()
                    .withLatLng(new LatLng(markers.get(i).getLatitude(), markers.get(i).getLongitude()))
                    .withData(markerData)
                    .withIconImage(icon)
                    .withIconSize(1.0f));
        }

        symbolManager.addClickListener(new OnSymbolClickListener() {
            @Override
            public boolean onAnnotationClick(Symbol symbol) {
                Switch lazyMode = (Switch) getActivity().findViewById(R.id.modeSwitcher);
                int id = symbol.getData().getAsJsonObject().get("id").getAsInt();
                String type = symbol.getData().getAsJsonObject().get("type").getAsString();

                //Check if lazymode switch is active and if the type is "UNI"
                if(!lazyMode.isChecked() && type.equals("uni")){
                    LatLng userLocation = new LatLng(getLastLocation().getLatitude(), getLastLocation().getLongitude());
                    //The distance between the marker and the user location
                    double distanceBetweenMarkerAndUser = userLocation.distanceTo(symbol.getLatLng());

                    //Check if the user is close to the symbol
                    if(distanceBetweenMarkerAndUser <= 10){
                        Intent intent = new Intent(getContext(),Information1.class);
                        intent.putExtra("Symbol information", id);
                        startActivity(intent);
                    }else{
                        redAlertMessage("You need to visit the campus and go near that marker to view the information");
                    }
                }else{
                    Intent intent = new Intent(getContext(),Information1.class);
//                    intent.putExtra("Symbol information",id);
                    intent.putExtra("Symbol information",id);
                    startActivity(intent);
                }
                return true;
            }
        });
    }

    //Method that return the last user location
    public Location getLastLocation(){
        return this.locationComponent.getLastKnownLocation();
    }

    //Handle the permission dialog response
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch(requestCode){
            case 12:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    onPermissionResult(true);
                }else{
                    onPermissionResult(false);
                }
        }
    }

    @Override
    public void onExplanationNeeded(List<String> permissionsToExplain) {
        //Toast.makeText(this.getContext(), "test", Toast.LENGTH_LONG).show();
    }

    //
    @Override
    public void onPermissionResult(boolean granted) {
        if (granted) {
            mapboxMap.getStyle(new Style.OnStyleLoaded() {
                @Override
                public void onStyleLoaded(@NonNull Style style) {
                    enableLocationComponent(style);
                }
            });
        } else {
            redAlertMessage("Switched to lazy mode");
            rejectedPermissionMode();
        }
    }

    public void rejectedPermissionMode(){
        Switch s = (Switch) getActivity().findViewById(R.id.modeSwitcher);
        ImageButton direction = getActivity().findViewById(R.id.userLocationButton);
        s.setChecked(true);
        s.setClickable(false);
        direction.setVisibility(View.GONE);

    }
    public void acceptedPermissionMode(){
        Switch s = (Switch) getActivity().findViewById(R.id.modeSwitcher);
        ImageButton direction = getActivity().findViewById(R.id.userLocationButton);
        s.setChecked(false);
        s.setClickable(true);
        direction.setVisibility(View.VISIBLE);
    }

    @Override
    @SuppressWarnings( {"MissingPermission"})
    public void onStart() {
        super.onStart();
        mapView.onStart();
        ((AppCompatActivity)getActivity()).getSupportActionBar().hide();
    }

    @Override
    public void onResume(){
        super.onResume();
        ((AppCompatActivity)getActivity()).getSupportActionBar().hide();
    }

    @Override
    public void onStop(){
        super.onStop();
        ((AppCompatActivity)getActivity()).getSupportActionBar().show();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }
}