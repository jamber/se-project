package com.example.belval2022.ui.events;

import android.app.Activity;
import android.app.Instrumentation;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.belval2022.R;
import com.example.belval2022.ui.map.MapFragment;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.Call;
import okhttp3.Callback;

import java.util.ArrayList;

public class EventsFragment extends Fragment {
    private ArrayList<Event> events;
    private RecyclerView recyclerView;
    private CustomAdapter.RecyclerViewClickListener listener;


    private EventsViewModel eventsViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        eventsViewModel = new ViewModelProvider(this).get(EventsViewModel.class);
        View root = inflater.inflate(R.layout.fragment_events, container, false);

        recyclerView = root.findViewById(R.id.recyclerview);
        recyclerView.setHasFixedSize(true);
        events = new ArrayList<>();
        setEvents();

        DividerItemDecoration divider = new DividerItemDecoration(this.getContext(),DividerItemDecoration.VERTICAL);
        recyclerView.addItemDecoration(divider);
        return root;
    }

    private void setAdapter(){
        setOnClickListener();
        CustomAdapter adapter = new CustomAdapter(events, listener);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this.getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
    }

    private void setOnClickListener() {
        listener = new CustomAdapter.RecyclerViewClickListener() {
            @Override
            public void OnClick(View v, int position) {
                Intent intent = new Intent(getActivity(), EventDetails.class);

                intent.putExtra("name", events.get(position).getName());
                intent.putExtra("description", events.get(position).getDescription());
                intent.putExtra("date", events.get(position).getDate());
                intent.putExtra("time", events.get(position).getLocation());
                intent.putExtra("latitude", events.get(position).getLatitude());
                intent.putExtra("longitude", events.get(position).getLongitude());
                startActivityForResult(intent, 1);
            }
        };
    }

    // gettig events data from the server
    private void setEvents(){

        //Http request
        OkHttpClient client = new OkHttpClient();
        String url = "http://10.0.2.2:3000/getAllEvents";

        Request request = new Request.Builder()
                .url(url)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if(response.isSuccessful()){
                    String myResponse = response.body().string();
                    try {
                        JSONArray a = new JSONArray(myResponse);
                        for (int i = 0; i < a.length(); i++){
                            JSONObject o = a.getJSONObject(i);

                            String eventName = o.getString("Event_Name");
                            String eventDate = o.getString("Event_Date");
                            String eventLocation = o.getString("Event_Location");
                            String eventDescription = o.getString("Event_Description");
                            double eventLat = o.getDouble("Lat");
                            double eventLong = o.getDouble("Long");

                            Event event = new Event(eventName, eventDate, eventLocation, eventDescription, eventLat, eventLong);
                            events.add(event);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    EventsFragment.this.getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            setAdapter();
                        }
                    });
                }
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1) {
            if(resultCode == Activity.RESULT_OK){
                double latitude = data.getDoubleExtra("lat", 1);
                double longitude = data.getDoubleExtra("long", 1);

                //Create a bundle to store the data
                Bundle bundle = new Bundle();
                bundle.putDouble("lat", latitude);
                bundle.putDouble("long", longitude);

                //Navigate to the map fragment
                Navigation.findNavController(getView()).navigate(R.id.navigation_map, bundle);
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }
    }
}






