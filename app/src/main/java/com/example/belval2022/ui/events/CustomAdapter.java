package com.example.belval2022.ui.events;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.belval2022.R;

import java.util.ArrayList;

public class CustomAdapter extends RecyclerView.Adapter<CustomAdapter.ViewHolder>  {

    private ArrayList<Event> events;
    private RecyclerViewClickListener listener;

    public CustomAdapter(ArrayList<Event> events, RecyclerViewClickListener listener) {
        this.events = events;
        this.listener = listener;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private TextView nameTxt;
        private TextView dateTxt;
        private TextView detailsTxt;

        public ViewHolder(final View view) {
            super(view);
            nameTxt = view.findViewById(R.id.textView);
            dateTxt = view.findViewById(R.id.textView2);
            detailsTxt = view.findViewById(R.id.descriptionTextView);

            view.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            listener.OnClick(v,getAdapterPosition());

        }
    }

    @NonNull
    @Override
    public CustomAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.events_list, parent, false);

        return new ViewHolder(itemView );

    }

    @Override
    public void onBindViewHolder(@NonNull CustomAdapter.ViewHolder holder, int position) {
        String name = events.get(position).getName();
        String date = events.get(position).getDate();
        holder.nameTxt.setText(name);
        holder.dateTxt.setText(date);

    }

    @Override
    public int getItemCount() {
        return events.size();
    }

    public interface RecyclerViewClickListener{
        void OnClick(View v,int position);
    }
}
