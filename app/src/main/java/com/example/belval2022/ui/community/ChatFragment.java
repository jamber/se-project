package com.example.belval2022.ui.community;

import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.example.belval2022.R;
import com.example.belval2022.ui.map.MapFragment;
import com.example.belval2022.ui.map.Marker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ChatFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ChatFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private ArrayList<ChatUser> chatItems = new ArrayList<ChatUser>();
    private RecyclerView recyclerView;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public ChatFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ChatFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ChatFragment newInstance(String param1, String param2) {
        ChatFragment fragment = new ChatFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }
    private void setAdapter(){
        ChatAdapter adapter = new ChatAdapter(chatItems);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
    }
    public void getChatMessages(){
        OkHttpClient client = new OkHttpClient();
        String url = "http://10.0.2.2:3000/getMessages";

        Request request = new Request.Builder()
                .url(url)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if(response.isSuccessful()){
                    String myResponse = response.body().string();
                    try {
                        JSONArray a = new JSONArray(myResponse);
                        for (int i = 0; i < a.length(); i++){
                            JSONObject o = a.getJSONObject(i);

                            String message = o.getString("Message");
                            int userId = o.getInt("Users_id");
                            JSONArray userInformations = o.getJSONArray("UserInfo");
                            JSONObject userInfo = userInformations.getJSONObject(0);

                            String username = userInfo.getString("User_name");
                            String avatar = userInfo.getString("Image_path");
                            System.out.println(avatar);
                            ChatUser chatItem = new ChatUser(username, message, avatar);
                            chatItems.add(chatItem);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    response.body().close();

                    ChatFragment.this.getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            setAdapter();
                        }
                    });
                }
            }
        });

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        ChatViewModel notificationsViewModel =
                new ViewModelProvider(this).get(ChatViewModel.class);
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_chat, container, false);

        notificationsViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(String s) {
                Button sendButton = (Button) getActivity().findViewById(R.id.send_button);
                EditText msgInput = (EditText) getActivity().findViewById(R.id.msg_input);
                recyclerView = getActivity().findViewById(R.id.recyclerChatView);

                getChatMessages();

                sendButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String msg = msgInput.getText().toString();
                        msgInput.setText("");
                        OkHttpClient client = new OkHttpClient();

                        String url = "http://10.0.2.2:3000/chat";
                        int SDK_INT = android.os.Build.VERSION.SDK_INT;
                        if (SDK_INT > 8) {
                            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                                    .permitAll().build();
                            StrictMode.setThreadPolicy(policy);
                            final MediaType MEDIA_TYPE_PNG = MediaType.parse("image/png");
                            SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(getContext());

                            RequestBody requestBody = new MultipartBody.Builder()
                                    .setType(MultipartBody.FORM)
                                    .addFormDataPart("username", pref.getString("username", "null"))
                                    .addFormDataPart("userId", pref.getString("userId", "null"))
                                    .addFormDataPart("message", msg)
                                    .build();

                            Request request = new Request.Builder()
                                    .url(url)
                                    .post(requestBody)
                                    .build();

                            Response response = null;
                            try {
                                response = client.newCall(request).execute();
                                chatItems.clear();
                                getChatMessages();
                                setAdapter();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });
            }
        });

        return root;
    }
}