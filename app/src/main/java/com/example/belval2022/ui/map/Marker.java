package com.example.belval2022.ui.map;

import com.example.belval2022.R;
import com.mapbox.mapboxsdk.geometry.LatLng;

public class Marker {
    private int id;
    private String type;
    private double latitude;
    private double longitude;
    private int icon;

    public Marker(int id, String type, double latitude, double longitude){
        this.id = Integer.valueOf(id);
        this.type = type;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public double getLatitude(){
        return this.latitude;
    }
    public double getLongitude(){
        return this.longitude;
    }
    public LatLng getMarkerCoordinates(){
        return new LatLng(latitude, longitude);
    }
    public String getType(){
        return this.type;
    }
    public int getId(){
        return this.id;
    }
    public int getIcon(){
        return this.icon;
    }
    @Override
    public String toString(){
        return this.id+" "+this.type+" "+this.latitude+" "+this.longitude;
    }
}
