package com.example.belval2022.ui.community;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.StrictMode;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

public class ChatUser {
    private String username;
    private String message;
    private String image;

    public ChatUser(String username, String message, String image){
        this.username = username;
        this.message = message;
        this.image = image;
    }

    public String getUsername(){
        return this.username;
    }
    public String getMessage(){
        return this.message;
    }
    public Bitmap getImage(){
        URL url = null;
        Bitmap bmp = null;

        try {
            int SDK_INT = android.os.Build.VERSION.SDK_INT;
            if (SDK_INT > 8) {
                StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                        .permitAll().build();
                StrictMode.setThreadPolicy(policy);
                url = new URL("http://10.0.2.2:3000" + this.image);
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        try {
            bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bmp;
    }
    public String toString(){ return this.username; }
}
