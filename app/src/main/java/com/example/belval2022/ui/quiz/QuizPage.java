package com.example.belval2022.ui.quiz;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.denzcoskun.imageslider.models.SlideModel;
import com.example.belval2022.Information1;
import com.example.belval2022.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class QuizPage extends AppCompatActivity {

     private String quiz;
     private static String answerCorrect =null ;
     private static String answerWrong1 =null;
     private static String answerWrong2 =null;
     private static int seconds = 0;
     private Timer timesChange = new Timer();
     private JSONArray a;
     private int randQuiz;
     private int totalSizeQuiz;

     public void onCreate(Bundle savedInstanceState)
     {
         Random random = new Random(System.currentTimeMillis());

         super.onCreate(savedInstanceState);
         setContentView(R.layout.activity_quiz_page);
         getSupportActionBar().setDisplayHomeAsUpEnabled(true);
         int quizInfo = getIntent().getIntExtra("Quiz Information",0);

         Button button1 = (Button)findViewById(R.id.answer1);
         button1.setBackgroundColor(Color.BLUE);
         Button button2 = (Button)findViewById(R.id.answer2);
         button2.setBackgroundColor(Color.BLUE);
         Button button3 = (Button)findViewById(R.id.answer3);
         button3.setBackgroundColor(Color.BLUE);
         TextView quizText = (TextView)findViewById(R.id.questionText);
         TextView counterQuiz = (TextView)findViewById(R.id.counterQuiz);

         Button nextQuestionQuiz = (Button)findViewById(R.id.button3);
         nextQuestionQuiz.setVisibility(View.INVISIBLE);

         //Similar to Information1 ( information page)
         OkHttpClient client = new OkHttpClient();
         String url = "http://10.0.2.2:3000/getQuizzes?id="+String.valueOf(quizInfo);

         Request request = new Request.Builder()
                 .url(url)
                 .build();
         client.newCall(request).enqueue(new Callback() {
             @Override
             public void onFailure(Call call, IOException e) {
                e.printStackTrace();
             }
             @RequiresApi(api = Build.VERSION_CODES.KITKAT)
             @Override
             public void onResponse(Call call, Response response) throws IOException {

                 if(response.isSuccessful()){
                     String myResponse = response.body().string();
                     try {
                         a = new JSONArray(myResponse);
                         totalSizeQuiz=a.length();

                         randQuiz = random.nextInt(a.length());

                         if(randQuiz<=a.length()) {

                             JSONObject o = a.getJSONObject(randQuiz);
                             quiz = o.getString("Question");
                             quizText.setText(quiz);

                             //We assign these strings the text of the different answers. There's always just one correct answer.
                             answerCorrect = o.getString("Correct_option");
                             answerWrong1 = o.getString("FirstWrong_option");
                             answerWrong2 = o.getString("SecondWrong_option");
                             counterQuiz.setText(a.length()+" questions remaining.");

                             //A user will only be able to click on a new question if he has answered the correct answer
                             nextQuestionQuiz.setOnClickListener(new View.OnClickListener(){

                                 @Override
                                 public void onClick(View v) {
                                     try {
                                         if(a.length()>1) {
                                             //we remove this question to avoid getting it again (if this button is pressed).
                                             a.remove(randQuiz);
                                             randQuiz= random.nextInt(a.length());

                                             //Similar to is above
                                             JSONObject o = a.getJSONObject(randQuiz);
                                             quiz = o.getString("Question");
                                             quizText.setText(quiz);
                                             nextQuestionQuiz.setVisibility(View.INVISIBLE);

                                             counterQuiz.setText(a.length()+" questions remaining.");
                                             answerCorrect = o.getString("Correct_option");
                                             answerWrong1 = o.getString("FirstWrong_option");
                                             answerWrong2 = o.getString("SecondWrong_option");

                                             //We have randomly assign the three buttons the different answers.
                                             int rand = random.nextInt(3)+1;
                                             if(rand == 1)
                                             {
                                                 button1.setText(answerCorrect);
                                                 button2.setText(answerWrong1);
                                                 button3.setText(answerWrong2);
                                             }else if(rand == 2) {
                                                 button2.setText(answerCorrect);
                                                 button1.setText(answerWrong1);
                                                 button3.setText(answerWrong2);
                                             } else if(rand == 3) {
                                                 button3.setText(answerCorrect);
                                                 button2.setText(answerWrong1);
                                                 button1.setText(answerWrong2);
                                             }
                                         }
                                         if(a.length()==1){
                                             counterQuiz.setText("Last question.");

                                             nextQuestionQuiz.setText("QUIT QUIZ");
                                             a.remove(randQuiz);
                                         }else if(a.length()<1) {
                                             finish();
                                         }
                                     }catch(Exception e){

                                     }

                                 }
                             });
                         }

                         //Similar to what happens when pressing the "next question" button, only this is run only one time when the user decides to do the quiz
                         int rand = random.nextInt(3)+1; //there are at most 3 answers
                         if(rand == 1)
                         {
                             button1.setText(answerCorrect);
                             button2.setText(answerWrong1);
                             button3.setText(answerWrong2);
                         }
                         else if(rand == 2)
                         {
                             button2.setText(answerCorrect);
                             button1.setText(answerWrong1);
                             button3.setText(answerWrong2);
                         }
                         else if(rand == 3)
                         {
                             button3.setText(answerCorrect);
                             button2.setText(answerWrong1);
                             button1.setText(answerWrong2);
                         }

                         button1.setOnClickListener(new View.OnClickListener() {
                             @Override
                             public void onClick(View v) {
                                 if(button1.getText().equals(answerCorrect)) //will make the nextQuestion button visible to the user.
                                 {
                                     if(nextQuestionQuiz.getVisibility()!=View.VISIBLE){
                                         nextQuestionQuiz.setVisibility(View.VISIBLE);
                                     }

                                     button1.setBackgroundColor(Color.GREEN); //change the color of the button for a second.
                                     try{
                                         timesChange.schedule(new TimerTask(){

                                             public void run(){
                                                 seconds++;

                                                 if (seconds == 1){
                                                     button1.setBackgroundColor(Color.BLUE);

                                                     seconds=0;
                                                 }
                                             }
                                         },1000);

                                     }catch(Exception e){

                                     }
                                 } else {
                                     button1.setBackgroundColor(Color.RED);  //change the color of the button for a second.
                                     try{
                                         timesChange.schedule(new TimerTask(){

                                             public void run(){
                                                 seconds++;

                                                 if (seconds == 1){
                                                     button1.setBackgroundColor(Color.BLUE);

                                                     seconds=0;
                                                 }
                                             }
                                         },1000);

                                     }catch(Exception e){

                                     }
                                 }
                             }
                         });

                         //similar to the first button
                         button2.setOnClickListener(new View.OnClickListener() {
                             @Override
                             public void onClick(View v) {
                                 if(button2.getText().equals(answerCorrect)) {
                                     if(nextQuestionQuiz.getVisibility()!=View.VISIBLE){
                                         nextQuestionQuiz.setVisibility(View.VISIBLE);
                                     }

                                     button2.setBackgroundColor(Color.GREEN);
                                     try{
                                         Timer timesChange = new Timer();
                                         timesChange.schedule(new TimerTask(){

                                             public void run(){
                                                 seconds++;

                                                 if (seconds == 1){
                                                    button2.setBackgroundColor(Color.BLUE);
                                                    seconds=0;
                                                 }
                                             }
                                         },1000);

                                     }catch(Exception e){

                                     }
                                 } else {
                                     button2.setBackgroundColor(Color.RED);
                                     try{
                                         timesChange.schedule(new TimerTask(){

                                             public void run(){
                                                 seconds++;

                                                 if (seconds == 1){
                                                     button2.setBackgroundColor(Color.BLUE);

                                                     seconds=0;
                                                 }
                                             }
                                         },1000);

                                     }catch(Exception e){

                                     }
                                 }

                             }
                         });

                         //similar to the other 2 buttons
                         button3.setOnClickListener(new View.OnClickListener() {
                             @Override
                             public void onClick(View v) {
                                 if(button3.getText().equals(answerCorrect))
                                 {
                                     if(nextQuestionQuiz.getVisibility()!=View.VISIBLE){
                                         nextQuestionQuiz.setVisibility(View.VISIBLE);
                                     }
                                     button3.setBackgroundColor(Color.GREEN);
                                     try{
                                         Timer timesChange = new Timer();
                                         timesChange.schedule(new TimerTask(){

                                             public void run(){
                                                 seconds++;

                                                 if (seconds == 1){
                                                     button3.setBackgroundColor(Color.BLUE);
                                                     seconds=0;
                                                 }
                                             }
                                         },1000);

                                     }catch(Exception e){

                                     }
                                 }
                                 else
                                 {
                                     button3.setBackgroundColor(Color.RED);
                                     try{
                                         timesChange.schedule(new TimerTask(){

                                             public void run(){
                                                 seconds++;

                                                 if (seconds == 1){
                                                     button3.setBackgroundColor(Color.BLUE);

                                                     seconds=0;
                                                 }
                                             }
                                         },1000);

                                     }catch(Exception e){

                                     }
                                 }

                             }
                         });
                     } catch (JSONException e) {
                         e.printStackTrace();
                     }
                 }
             }
         });
     }
}
