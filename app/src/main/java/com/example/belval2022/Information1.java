package com.example.belval2022;

import com.denzcoskun.imageslider.ImageSlider;
import com.denzcoskun.imageslider.models.SlideModel;
import com.example.belval2022.ui.quiz.QuizPage;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class Information1 extends AppCompatActivity  {

    private String description;
    private String imagesDescription;
    private String nameOfDescription;
    private String openingHours;
    private String daysString="";
    private String hoursString="";
    private ArrayList<String> imageURL = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_information1);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        TextView textInformation = (TextView)findViewById(R.id.textView);
        TextView openingDaysText = (TextView)findViewById(R.id.openingDaysText);
        TextView openingHoursText = (TextView)findViewById(R.id.openingHoursText);
        Button quizButton = (Button)findViewById(R.id.quizButton);

        //The information is passed when a user clicks on a specific marker.
        int sessionInfo = getIntent().getIntExtra("Symbol information",0);
        ImageSlider imageSlider = findViewById(R.id.slider);
        List<SlideModel> slideModels = new ArrayList<>();

        //we load the database.
        OkHttpClient client = new OkHttpClient();
        String url = "http://10.0.2.2:3000/getBuildingInformation?id="+String.valueOf(sessionInfo);

        Request request = new Request.Builder()
                .url(url)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if(response.isSuccessful()){
                    String myResponse = response.body().string();
                    try {
                        JSONObject a = new JSONObject(myResponse);
                        description = a.getString("Description");
                        nameOfDescription=a.getString("Name");
                        imagesDescription=a.getString("images");
                        openingHours = a.getString("OpeningHours");
                        String icon = a.getString("Type");

                        /*depending whether the marker corresponds to a building belonging to the University or not,
                        since only cultural and uni buildings have quizzes, we decide to display the button for the quiz or not
                         */
                        if(icon.equals("uni") || icon.equals("Culture")){
                            quizButton.setVisibility(View.VISIBLE);
                        }else{
                            quizButton.setVisibility(View.GONE);
                        }

                        //there could be several images depending of the marker ( Uni buildings have more than 1 image)
                        if(imagesDescription!=null) {
                            JSONArray b = new JSONArray(imagesDescription);

                            for (int i = 0; i < b.length(); i++) {
                                JSONObject o = b.getJSONObject(i);
                                imageURL.add(o.getString("Path"));
                            }
                        }

                        //display the opening times in their respective text views
                        if(openingHours!=null){
                            JSONArray c = new JSONArray(openingHours);

                            for (int i = 0; i < c.length(); i++) {
                                JSONObject o = c.getJSONObject(i);
                                daysString += o.getString("weekday")+"\n";
                                hoursString += o.getString("Opening_Time")+"\n";
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                Information1.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //we use this slide provided by https://github.com/smarteist/Android-Image-Slider
                        if (!imageURL.isEmpty()) {
                            for (int i = 0; i < imageURL.size(); i++) {
                                slideModels.add(new SlideModel("http://10.0.2.2:3000/" + imageURL.get(i), nameOfDescription));
                                imageSlider.setImageList(slideModels, true);

                            }
                        }
                        textInformation.setText(description);
                        openingDaysText.setText(daysString);
                        openingHoursText.setText(hoursString);
                    }
                });
            }
        });

        quizButton.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), QuizPage.class);
                i.putExtra("Quiz Information",sessionInfo);
                startActivity(i);
            }
        });
    }

}